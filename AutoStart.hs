{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module AutoStart where

import Utility.FreeDesktop
import System.Info
import System.Environment
import System.Directory

installAutoStartFile :: IO ()
installAutoStartFile = go =<< autoStartFile
  where
	go (Just f) = case os of
		"linux" -> installFdoAutoStart f
		"freebsd" -> installFdoAutoStart f
		_ -> return ()
	go Nothing = return ()

isAutoStartFileInstalled :: IO Bool
isAutoStartFileInstalled = maybe (pure False) doesFileExist =<< autoStartFile

autoStartFile :: IO (Maybe FilePath)
autoStartFile = case os of
	"linux" -> Just . autoStartPath "keysafe" <$> userConfigDir
	_ -> return Nothing

installFdoAutoStart :: FilePath -> IO ()
installFdoAutoStart f = do
	command <- getExecutablePath
	writeDesktopMenuFile (fdoAutostart command) f

fdoAutostart :: FilePath -> DesktopEntry
fdoAutostart command = genDesktopEntry
        "Keysafe"
        "Autostart"
        False
        (command ++ " --autostart")
        Nothing
        []
