{-# LANGUAGE OverloadedStrings, GeneralizedNewtypeDeriving, MultiParamTypeClasses, FlexibleInstances, DeriveGeneric #-}

{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Types where

import Types.Cost
import qualified Data.ByteString as B
import qualified Data.Text as T
import Data.String
import Control.DeepSeq
import GHC.Generics (Generic)
import Data.Aeson

-- | keysafe stores secret keys.
newtype SecretKey = SecretKey B.ByteString
	deriving (Eq)

-- | The secret key, encrypted with a password, in fixed size chunks.
data EncryptedSecretKey = EncryptedSecretKey [B.ByteString] (CostCalc BruteForceOp UnknownPassword)

instance NFData EncryptedSecretKey where
	rnf (EncryptedSecretKey cs _) = rnf cs

instance Show EncryptedSecretKey where
	show (EncryptedSecretKey cs _) = show cs

instance Bruteforceable EncryptedSecretKey UnknownPassword where
	getBruteCostCalc (EncryptedSecretKey _ cc) = cc

-- | An object in a form suitable to be stored on a keysafe server.
newtype StorableObject = StorableObject { fromStorableObject :: B.ByteString }
	deriving (Show, Eq, Ord, Generic)

-- | An identifier for a StorableObject
newtype StorableObjectIdent = StorableObjectIdent B.ByteString
	deriving (Show, Eq, Ord, NFData)

-- | A Shamir secret share, with a known number (N of M).
data Share = Share ShareNum StorableObject
	deriving (Eq, Ord)

type ShareNum = Int

-- | A password used to encrypt a key stored in keysafe.
newtype Password = Password B.ByteString
	deriving (IsString)

-- | A name associated with a key stored in keysafe.
newtype Name = Name B.ByteString
	deriving (Eq, Show, Monoid, Semigroup)

-- | Source of the secret key stored in keysafe.
data SecretKeySource = GpgKey KeyId | KeyFile FilePath
	deriving (Show, Eq, Generic)

instance ToJSON SecretKeySource
instance FromJSON SecretKeySource

-- | The keyid is any value that is unique to a private key, and can be
-- looked up somehow without knowing the private key.
--
-- A gpg keyid is the obvious example.
data KeyId = KeyId T.Text
	deriving (Show, Eq, Generic)

instance ToJSON KeyId
instance FromJSON KeyId
