{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module HTTP.Logger where

import System.Log.FastLogger
import Data.String

data Logger = Logger LoggerSet LoggerSet

newLogger :: IO Logger
newLogger = Logger
	<$> newStdoutLoggerSet defaultBufSize
	<*> newStderrLoggerSet defaultBufSize

logStdout :: Logger -> String -> IO ()
logStdout (Logger l _) = sendLogger l

logStderr :: Logger -> String -> IO ()
logStderr (Logger _ l) = sendLogger l

sendLogger :: LoggerSet -> String -> IO ()
sendLogger l s = pushLogStrLn l (fromString s)
