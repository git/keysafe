{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

{-# LANGUAGE RankNTypes #-}

module Types.UI where

import Types

data UI = UI
	{ isAvailable :: IO Bool
	, showError :: Desc -> IO ()
	, showInfo :: Title -> Desc -> IO ()
	, promptQuestion :: Title -> Desc -> Question -> IO Bool
	, promptName :: Title -> Desc -> Maybe Name -> (Name -> Maybe Problem) -> IO (Maybe Name)
	, promptPassword :: Bool -> Title -> Desc -> IO (Maybe Password)
	, promptKeyId :: Title -> Desc -> [(Name, KeyId)] -> IO (Maybe KeyId)
	, withProgress :: forall a. Title -> Desc -> ((Percent -> IO ()) -> IO a) -> IO a
	}

type Title = String
type Desc = String
type Percent = Int
type Problem = String
type Question = String
