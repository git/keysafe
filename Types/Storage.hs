{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Types.Storage where

import Types
import Types.Server
import GHC.Generics
import Data.Aeson.Types

-- | All known locations where shares can be stored, ordered with
-- preferred locations first.
newtype StorageLocations = StorageLocations [Storage]
	deriving (Monoid, Semigroup)

newtype LocalStorageDirectory = LocalStorageDirectory FilePath

data StorageLevel = LocallyPreferred | Recommended | Alternate | Untrusted
	deriving (Show, Eq, Ord, Bounded, Enum)

-- | Storage interface. This can be used both for local storage,
-- an upload queue, or a remote server.
--
-- Note that there is no interface to enumerate shares.
-- This is intentional; servers should not allow that.
data Storage = Storage
	{ storeShare :: StorableObjectIdent -> Share -> IO StoreResult
	, retrieveShare :: ShareNum -> StorableObjectIdent -> IO RetrieveResult
	, obscureShares :: IO ObscureResult
	-- ^ Run after making some calls to storeShare/retrieveShare,
	-- to avoid correlation attacks.
	, countShares :: IO CountResult
	, moveShares :: Storage -> IO [StoreResult]
	-- ^ Tries to move all shares from this storage to another one.
	, uploadQueue :: Maybe Storage
	, storageLevel :: StorageLevel
	, getServer :: Maybe Server
	}

data StoreResult = StoreSuccess | StoreAlreadyExists | StoreFailure String
	deriving (Show, Generic)

data RetrieveResult = RetrieveSuccess Share | RetrieveFailure String
	deriving (Generic)

data ObscureResult = ObscureSuccess | ObscureFailure String
	deriving (Show, Generic)

data CountResult = CountResult Integer | CountFailure String
	deriving (Show, Generic)

instance ToJSON StoreResult
instance FromJSON StoreResult
instance ToJSON CountResult
instance FromJSON CountResult
