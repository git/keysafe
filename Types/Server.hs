{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

{-# LANGUAGE DeriveGeneric #-}

module Types.Server (module Types.Server, Port) where

import Data.Aeson
import GHC.Generics
import Network.Wai.Handler.Warp (Port)

type HostName = String

-- | Server address can use either tor .onion addresses, or regular
-- hostnames. Using tor is highly recommended, to avoid correlation
-- attacks.
data ServerAddress = ServerAddress HostName Port
	deriving (Show, Eq, Ord)

-- | Name used in queuing uploads to the server. Should remain stable
-- across keysafe versions.
newtype ServerName = ServerName { fromServerName :: String }
	deriving (Show, Eq, Ord, Generic)

instance ToJSON ServerName
instance FromJSON ServerName

data Server = Server
	{ serverName :: ServerName
	, serverAddress :: [ServerAddress]
	-- ^ A server may have multiple addresses, or no current address.
	, serverDesc :: String
	}
	deriving (Show, Eq, Ord)
