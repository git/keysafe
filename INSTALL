## Quick installation

This installs keysafe to ~/.local/bin, and is sufficient to use keysafe
to back up or restore your private key.

First install Haskell's stack tool, ghc, the zlib, g++, readline and argon2
libraries. Also, zenity is needed to use keysafe's GUI. 
For example, on a Debian testing system:

	sudo apt-get install haskell-stack ghc \
		zlib1g-dev g++ libreadline-dev libargon2-0-dev zenity

If your distribution does not yet include Haskell's stack tool,
see <http://haskellstack.org/>

If your distribution does not yet include argon2, see 
<https://github.com/P-H-C/phc-winner-argon2>

Then to build and install keysafe, run this in the keysafe directory:

	stack install

Note that there is a manpage, but stack doesn't install it yet.

## System-wide installation

This installs keysafe in /usr/bin, and includes the man page, 
desktop file, autostart file, systemd service file, init script, etc.

Start by installing the dependencies as shown in Quick installation.

Then, in the keysafe directory:

	make
	sudo make install

## Packaging

You will probably want to use the Makefile.
Set PREFIX to install to a different location.
Set BUILDER=cabal to use cabal rather than the default stack to build.

The make install target creates a keysafe user. Use the install-files
target to avoid doing that at package build time. You may create the
keysafe user at package install time instead, although it is only used
by the keysafe server.

While keysafe ships with a systemd service file and an init script,
distributions should not enable it to be started by default.
(Or can put it in its own keysafe-server package.)
