`keysafe` and `zxcvbn-c` (and possibly others) fail to build with Cabal 2.0. Cabal now requires a top-level `custom-setup` and inner `setup-depends` stanza for packages with build-type `Custom`. This field specifies the dependencies of `Setup.hs` itself.

For example:

    custom-setup:
      setup-depends:
        base,
        Cabal

See https://github.com/haskell/cabal/issues/5278 for someone complaining about the horrible error message Cabal spits out in this situation.
