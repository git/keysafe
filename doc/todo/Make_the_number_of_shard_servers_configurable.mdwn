Currently 3 shard servers are used for saving the key and either two of them are required to reconstruct the key.

In my opinion it's too easy for any two subjects to cooperate when it comes to key reconstruction. I'd like to therefore have the number of shard servers configurable (with possibly hundreds of shard servers). This implies also having configurable also the minimum number of servers needed for key reconstruction (with possibly hundreds of required pieces of the key).
