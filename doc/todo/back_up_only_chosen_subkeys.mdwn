I'm a DD, so I don't want keysafe to ever back up my signing subkey nor my master key.  But I'd like it to back up my encryption subkey, so that I don't lose access to (e.g.) git-annex special remotes encrypted against that key.  It would be good if keysafe asked me which subkeys I want to back up.  --spwhitton
 
