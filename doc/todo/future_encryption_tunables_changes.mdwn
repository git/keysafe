If switching any of the encryption tunables for some reason,
consider making these changes all at once:

* Argon2d is more resistent to GPU/ASIC attack optimisation.
  Switching from Argon2i would require new tunables, and delay restores
  (of keys backed up using the old tunables, and when the user provides the
  wrong name) by ~10 minutes, so deferred for now
  until there's some other reason to change the tunables.
* The ShareIdents derivation currently appends a number and sha256 hashes
  to generate a stream of values. Ben M points out that HMAC is a more
  typical way to do such a thing. Even better, a HKDF-Expand
  (RFC5869) can generate a stream which can then be chunked up into values.
  Either of these would avoid a full pre-image attack on SHA-2 breaking
  keysafe. Of course, such an SHA-2 attack would be a general security
  disaster. HKDF may prove more robust in the face of partial SHA-2 breaks.
  Deferred for now until tthere's some other reason to change keysafe's
  tunables.
* Perhaps use CHACHA2 instead of AES?
