keysafe 0.20200214 released with [[!toggle text="these changes"]]
[[!toggleable text="""
   * Updated many dependencies.
   * Support building with ghc 8.x.
   * Stackage lts-14.25.
   * Downgrade purism's keysafe server from recommended to alternate,
     mostly because the server is down, and AFAIK has been down for years,
     and I don't currently know if they plan to ever provide it again."""]]