keysafe 0.20170122 released with [[!toggle text="these changes"]]
[[!toggleable text="""
   * Adjust cabal bounds to allow building with ghc 8.0.
     However, the stack.yaml is still using an old LTS version
     to avoid polynomial's failure to build with ghc 8.0
     (https://github.com/mokus0/polynomial/issues/8)
   * Clarify that dollars in cost estimates are USD.
   * Keysafe has a new website, https://keysafe.branchable.com/"""]]