# The install target will add this before all paths it writes to.
PREFIX?=

# Can be "stack" or "cabal", or "./Setup" to build and use Setup.hs 
BUILDER?=stack

# Options to pass to the BUILDER.
# Using -j1 may result in a reproducible build.
BUILDEROPTIONS?=

# Propigate flags through ghc to linker and compiler.
ghc_options=$(shell \
	for w in $(LDFLAGS); do \
	       printf -- "-optl%s\n" "$$w"; \
	done; \
	for w in $(CFLAGS); do \
	       printf -- "-optc%s\n" "$$w"; \
	done; \
	for w in $(CPPFLAGS); do \
	       printf -- "-optc-Wp,%s\n" "$$w"; \
	done; \
	)

build:
	rm -f keysafe
	$(MAKE) keysafe

keysafe:
	if [ "$(BUILDER)" = ./Setup ]; then ghc --make Setup; fi
	if [ "$(BUILDER)" = stack ]; then \
		$(BUILDER) build --ghc-options="$(ghc_options)" $(BUILDEROPTIONS); \
	else \
		$(BUILDER) configure --ghc-options="$(ghc_options)"; \
		$(BUILDER) build $(BUILDEROPTIONS); \
	fi
	if [ "$(BUILDER)" = stack ]; then \
		ln -sf $$(stack path --dist-dir)/build/keysafe/keysafe keysafe; \
	else \
		ln -sf dist/build/keysafe/keysafe keysafe; \
	fi

clean:
	if [ "$(BUILDER)" != ./Setup ] && [ "$(BUILDER)" != cabal ]; then $(BUILDER) clean; fi
	rm -rf keysafe dist .stack-work Setup Setup.hi Setup.o

install: install-files
	useradd --system keysafe
	chmod 700 $(PREFIX)/var/lib/keysafe
	chown keysafe:keysafe $(PREFIX)/var/lib/keysafe

install-files: keysafe
	install -d $(PREFIX)/var/lib/keysafe
	install -d $(PREFIX)/usr/bin
	install -s -m 0755 keysafe $(PREFIX)/usr/bin/keysafe
	install -d $(PREFIX)/usr/share/man/man1
	install -m 0644 keysafe.1 $(PREFIX)/usr/share/man/man1/keysafe.1
	install -d $(PREFIX)/lib/systemd/system
	install -m 0644 keysafe.service $(PREFIX)/lib/systemd/system/keysafe.service
	install -d $(PREFIX)/etc/init.d
	install -m 0755 keysafe.init $(PREFIX)/etc/init.d/keysafe
	install -d $(PREFIX)/etc/default
	install -m 0644 keysafe.default $(PREFIX)/etc/default/keysafe
	install -d $(PREFIX)/usr/share/applications/
	install -m 0644 keysafe.desktop $(PREFIX)/usr/share/applications/keysafe.desktop
	install -d $(PREFIX)/etc/xdg/autostart/
	install -m 0644 keysafe.autostart $(PREFIX)/etc/xdg/autostart/keysafe.desktop
