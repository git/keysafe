{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Servers where

import Types.Server
import Types.Storage
import Storage.Network

-- | Keysafe's server list.
--
-- Note: Avoid removing servers from this list, as that will break
-- restores. If necessary, a server can be set to Untrusted to prevent
-- uploads to it.
--
-- Also, avoid changing the ServerName of any server, as that will
-- cause any uploads queued under that name to not go through.
serverList :: Maybe LocalStorageDirectory -> [Storage]
serverList d =
	[ mk Alternate $ Server (ServerName "keysafe.joeyh.name")
		[ServerAddress "vzgrspuxbtnlrtup.onion" 4242]
		"Provided by Joey Hess. Digital Ocean VPS, located in Indonesia"

	, mk Alternate $ Server (ServerName "keysafe.puri.sm")
		[ServerAddress "hlmjmeth356s5ekm.onion" 4242]
		"Provided by Purism. Located in the EU (Cyprus)"
		-- Note that while Joey Hess was employed by Purismat one
		-- point, he never had access to this server or its data,
		-- and Purism has policy to never allow him such access.
		-- This is important since he runs keysafe.joeyh.name.

	-- still being vetted
	, mk Alternate $ Server (ServerName "thirdserver")
		[ServerAddress "eqi7glyxe5ravak5.onion" 4242]
		"Provided by Marek Isalski at Faelix. Currently located in UK, but planned move to CH"
	]
  where
	mk l s = networkStorage l d s
