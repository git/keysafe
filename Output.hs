-- All console output in keysafe should go via this module;
-- avoid using putStrLn, print, etc directly.

module Output (ask, progress, say, warn, display) where

import System.IO
import Data.Char

ask :: String -> IO ()
ask s = do
	putStr (escape s)
	hFlush stdout

progress :: String -> IO ()
progress = ask

say :: String -> IO ()
say = putStrLn . escape

warn :: String -> IO ()
warn = hPutStrLn stderr . escape

display :: Show s => s -> IO ()
display = say . show

-- | Prevent malicious escape sequences etc in a string
-- from being output to the console.
escape :: String -> String
escape = concatMap go
   where
	go c = if isPrint c || isSpace c
		then [c]
		else "\\" ++ show (ord c)
