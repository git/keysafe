{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module SecretKey where

import Types
import Share
import qualified Gpg
import qualified Data.ByteString as B
import System.IO
import System.Posix.IO

getSecretKey :: SecretKeySource -> IO SecretKey
getSecretKey (GpgKey kid) = Gpg.getSecretKey kid
getSecretKey (KeyFile f) = SecretKey <$> B.readFile f

-- | Can throw exception if the secret key already exists.
writeSecretKey :: Distinguisher -> SecretKey -> IO ()
writeSecretKey (Distinguisher (GpgKey _)) secretkey = Gpg.writeSecretKey secretkey
writeSecretKey AnyGpgKey secretkey = Gpg.writeSecretKey secretkey
writeSecretKey (Distinguisher (KeyFile f)) (SecretKey b) = do
	fd <- openFd f WriteOnly (Just 0o666)
		(defaultFileFlags { exclusive = True } )
	h <- fdToHandle fd
	B.hPut h b
	hClose h
