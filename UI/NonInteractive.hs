{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module UI.NonInteractive (noninteractiveUI) where

import Types.UI
import Output
import Control.Exception

noninteractiveUI :: UI
noninteractiveUI = UI
	{ isAvailable = return True
	, showError = myShowError
	, showInfo = myShowInfo
	, promptQuestion = myPrompt
	, promptName = \t d _ -> myPrompt t d
	, promptPassword = \b t d -> myPrompt t d b
	, promptKeyId = myPrompt
	, withProgress = myWithProgress
	}

myShowError :: Desc -> IO ()
myShowError desc = warn $ "Error: " ++ desc

myShowInfo :: Title -> Desc -> IO ()
myShowInfo _title desc = say desc

myPrompt :: Title -> Desc -> x -> IO a
myPrompt _title desc _ = do
	say desc
	error "Not running at a terminal and zenity is not installed; cannot interact with user."

myWithProgress :: Title -> Desc -> ((Percent -> IO ()) -> IO a) -> IO a
myWithProgress _title desc a = bracket_ setup cleanup (a sendpercent)
  where
	setup = say desc
	sendpercent p = progress (show p ++ "%  ")
	cleanup = say "done"
