{- Copyright 2016 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Entropy where

import Types
import Types.Cost
import qualified Data.ByteString.UTF8 as B
import Text.Password.Strength (estimate, UserDict)

-- | Calculation of the entropy of a password.
-- Uses zxcvbn so takes word lists, and other entropy weakening problems
-- into account.
calcPasswordEntropy :: Password -> UserDict -> Entropy UnknownPassword
calcPasswordEntropy (Password p) userdict = Entropy $ floor $
	estimate (B.toString p) userdict

-- | Naive calculation of the entropy of a name.
-- Assumes that the attacker is not targeting a particular list of names.
calcNameEntropy :: Name -> Entropy UnknownName
calcNameEntropy (Name n) = Entropy $ floor $
	estimate (B.toString n) []
